﻿using System;
using Models.Extensions;

namespace Models
{
    public class LogEntry
    {
        public LogEntry(DateTime date, string message, LogFile logFile)
        {
            Date = date;
            Message = message;
            LogFile = logFile;
        }

        public long Id { get; set; }
        public DateTime Date { get; }
        public string Message { get; }
        public LogFile LogFile { get; }
        public override string ToString() => $"{Date:yyyy-MM-dd HH:mm:ss.fff} {Message}";
    }
}