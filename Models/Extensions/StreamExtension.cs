﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Extensions
{
    public static class StreamExtension
    {
        public static string Read(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static async Task<Log> BuildLogAsync(this IEnumerable<StreamInfo> fileStreams)
        {
            var tasks = fileStreams.Select(
                    stream =>
                    {
                        return stream.StreamReader.ReadToEndAsync()
                            .ContinueWith(
                                t => new LogFile(stream.Path, t.Result));
                    })
                .ToArray();


            await Task.WhenAll(tasks);
            var results = tasks
                .SelectMany(x => x.Result.Entries)
                .OrderBy(x => x.Date)
                .ToList();

            var id = 0;
            results.ForEach(x => x.Id = id++);
            return new Log(results);
        }

    }
}