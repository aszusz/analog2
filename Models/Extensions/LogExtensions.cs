﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Models.LogicRulesEngine;

namespace Models.Extensions
{
    internal static class LogExtensions
    {

        internal static IEnumerable<LogEntry> ToEntries(this LogFile logFile)
        {
            var datePattern = @"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d";
            var regexPattern = string.Format(@"(?<date>{0})( )(?<text>[\S\s]*?(?={0})|[\S\s]*)", datePattern);
            var regex = new Regex(regexPattern, RegexOptions.Compiled);
            return
                from Match match in regex.Matches(logFile.Content)
                select new LogEntry(
                    Convert.ToDateTime(match.Groups["date"].ToString()),
                    match.Groups["text"].ToString(),
                    logFile);

        }

        internal static IEnumerable<LogEntry> Filter(this IEnumerable<LogEntry> entries, IEvaluable<LogEntry> filter)
        {
            return entries.Where(filter.Evaluate);
        }

        internal static async Task<IEnumerable<LogEntry>> FilterAsync(this IEnumerable<LogEntry> entries,
            IEvaluable<LogEntry> filter)
        {
            return await Task.Run(() => entries.Filter(filter));
        }

        internal static string ConvertToString(this IList<LogEntry> entries)
        {
            var sb = new StringBuilder();
            foreach (var entry in entries)
            {
                var text = entry.ToString();
                sb.Append(entry);
            }

            return sb.ToString();
        }
    }
}