using System.Collections.Generic;
using System.Linq;

namespace Models
{
    public class LogFileComparer : IComparer<LogFile>
    {
        public int Compare(LogFile firstPart, LogFile secondPart)
        {
            if (firstPart == null || secondPart == null) return 0;
            if (!firstPart.Entries.Any() || !secondPart.Entries.Any()) return 0;
            if (firstPart.Entries.Last().Date <= secondPart.Entries.First().Date) return -1;
            if (firstPart.Entries.First().Date >= secondPart.Entries.Last().Date) return 1;
            return 0;
        }
    }
}