﻿using System.IO;

namespace Models
{
    public class StreamInfo
    {
        public StreamReader StreamReader { get; set; }
        public string Path { get; set; }
    }
}