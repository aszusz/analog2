﻿using System;
using Models.LogicRulesEngine;

namespace Models.Rules
{
    public class IdRule : RuleBase
    {
        public IdRuleAction SelectedAction;
        public long Id { get; set; }


        public override IEvaluable<LogEntry> GetFilter()
        {
            if (!IsEnabled)
            {
                return LogicRule<LogEntry>.Pass;
            }

            switch (SelectedAction)
            {
                case IdRuleAction.Before:
                    return GetBeforeRule();
                case IdRuleAction.After:
                    return GetAfterRule();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private LogicRule<LogEntry> GetBeforeRule()
        {
            return new LogicRule<LogEntry>(entry => entry.Id <= Id);
        }

        private LogicRule<LogEntry> GetAfterRule()
        {
            return new LogicRule<LogEntry>(entry => entry.Id >= Id);
        }
    }
}