﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.LogicRulesEngine;

namespace Models
{
    public interface ILogService
    {
        Task<Log> LoadAsync(IEnumerable<StreamInfo> fileStreams);
        Task<LogResult> FilterAsync(Log log, IEvaluable<LogEntry> filter);

    }
}
