﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Extensions;

namespace Models
{
    public class Log
    {

        public Log(IList<LogEntry> entries)
        {
            Entries = entries;
        }

        public IList<LogEntry> Entries { get; }

        public override string ToString()
        {
            return Entries.ConvertToString();
        }
    }
}