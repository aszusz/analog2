﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models.Extensions;

namespace Models
{
    public class LogFile
    {
        public LogFile(string path, string content)
        {

            Path = path;
            Content = content;
            Entries = this.ToEntries().ToList();
        }
        public string Path { get; }
        public string Content { get; }
        public IList<LogEntry> Entries { get; }
        internal long Id { get; set; }
    }
}