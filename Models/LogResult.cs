﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
    public class LogResult
    {
        internal LogResult(IList<LogEntry> entries)
        {
            Entries = entries;
            var sb = new StringBuilder();
            var startLine = 1;
            _entryLines = new List<Tuple<int, int>>();

            foreach (var entry in entries)
            {
                var text = entry.ToString();
                var lines = text.Count(c => c == '\n');
                _entryLines.Add(new Tuple<int, int>(startLine, startLine + lines));
                startLine += lines;
                sb.Append(text);
            }
            LogText = sb.ToString();

        }

        public IList<LogEntry> Entries { get; }
        private List<Tuple<int, int>> _entryLines { get; }
        public string LogText { get; }

        public LogEntry GetLogFromLine(int line)
        {
            var min = 0;
            var max = Entries.Count - 1;
            while (min <= max)
            {
                var mid = (min + max) / 2;
                var item = _entryLines[mid];

                if (line >= item.Item1 && line < item.Item2)
                {
                    return Entries[mid];
                }
                else if (line < _entryLines[mid].Item1)
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }

            }
            return null;
        }

        public bool IsLineStartingLogEntry(int line)
        {
            var min = 0;
            var max = Entries.Count - 1;
            while (min <= max)
            {
                var mid = (min + max) / 2;
                var item = _entryLines[mid];

                if (line == item.Item1)
                {
                    return true;
                }
                else if (line > item.Item1 && line < item.Item2)
                {
                    return false;
                }
                else if (line < _entryLines[mid].Item1)
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }

            }
            return false;
        }

    }
}