﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Models.Extensions;
using Models.LogicRulesEngine;

namespace Models
{
    public class LogService : ILogService
    {
        public async Task<Log> LoadAsync(IEnumerable<StreamInfo> fileStreams)
        {
            return await fileStreams.BuildLogAsync();
        }

        public async Task<LogResult> FilterAsync(Log log, IEvaluable<LogEntry> filter)
        {
            var logs = log != null
                ? await log.Entries.FilterAsync(filter)
                : Enumerable.Empty<LogEntry>();
            return new LogResult(logs.ToList());
        }
    }
}