﻿using System;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;

namespace Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : MetroWindow
    {
        public ShellView()
        {
            InitializeComponent();
        }
    }
}
