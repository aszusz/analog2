﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;
using Caliburn.Micro;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;

namespace Views.Behaviors
{
    public class LineTransformersBehavior : Behavior<TextEditor>
    {
        public static readonly DependencyProperty LineTransformersProperty = DependencyProperty.Register(
            "LineTransformers", typeof(IEnumerable<IVisualLineTransformer>), typeof(LineTransformersBehavior), new PropertyMetadata(default(IEnumerable<IVisualLineTransformer>), LineTransformersChanged));

        private static void LineTransformersChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var behavior = (LineTransformersBehavior)dependencyObject;

            var observable = dependencyPropertyChangedEventArgs.OldValue as INotifyCollectionChanged;
            if (observable != null)
            {
                observable.CollectionChanged -= behavior.OnCollectionChanged;
            }

            behavior.UpdateTransformers(dependencyPropertyChangedEventArgs.NewValue as IEnumerable<IVisualLineTransformer>);
        }

        public IEnumerable<IVisualLineTransformer> LineTransformers
        {
            get { return (IEnumerable<IVisualLineTransformer>)GetValue(LineTransformersProperty); }
            set { SetValue(LineTransformersProperty, value); }
        }

        protected override void OnAttached()
        {
            UpdateTransformers(LineTransformers);
        }

        private void UpdateTransformers(IEnumerable<IVisualLineTransformer> transformers)
        {
            AssociatedObject.TextArea.TextView.LineTransformers.Clear();

            if (transformers == null)
            {
                return;
            }

            foreach (var t in transformers)
            {
                AssociatedObject.TextArea.TextView.LineTransformers.Add(t);
            }

            var observable = transformers as INotifyCollectionChanged;
            if (observable != null)
            {
                observable.CollectionChanged += OnCollectionChanged;
            }
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            switch (notifyCollectionChangedEventArgs.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var x in notifyCollectionChangedEventArgs.NewItems.Cast<IVisualLineTransformer>())
                    {
                        AssociatedObject.TextArea.TextView.LineTransformers.Add(x);
                    }
                    return;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var x in notifyCollectionChangedEventArgs.OldItems.Cast<IVisualLineTransformer>())
                    {
                        AssociatedObject.TextArea.TextView.LineTransformers.Remove(x);
                    }
                    return;
                case NotifyCollectionChangedAction.Reset:
                    AssociatedObject.TextArea.TextView.LineTransformers.Clear();
                    foreach (var x in LineTransformers)
                    {
                        AssociatedObject.TextArea.TextView.LineTransformers.Add(x);
                    }
                    return;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected override void OnDetaching()
        {
            var observable = LineTransformers as INotifyCollectionChanged;
            if (observable != null)
            {
                observable.CollectionChanged -= OnCollectionChanged;
            }
        }
    }
}
