﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Caliburn.Micro;
using ICSharpCode.AvalonEdit.Rendering;
using MaterialDesignThemes.Wpf;
using Models;
using Models.Rules;
using ViewModels.ElementGenerators;
using ViewModels.LineTransformers;
using ViewModels.Messages;
using ViewModels.Services;
using SnackbarMessage = ViewModels.Messages.SnackbarMessage;
using GongSolutions.Wpf.DragDrop;
using ViewModels.Rules;

namespace ViewModels
{
    public class ShellViewModel : PropertyChangedBase, IHandle<SnackbarMessage>, IHandle<LoadRuleMessage>, IHandle<SaveRuleMessage>, IDropTarget
    {
        private readonly IFileService _fileService;
        private readonly ISyntaxHighlightingManager _syntaxHighlightingManager;
        private readonly ILogService _logService;
        private readonly IEventAggregator _eventAggregator;
        private readonly SnackbarService _snackbarService;
        private readonly IDialogService _dialogService;
        private readonly Func<ProgressDialogViewModel> _progressDialogFactory;
        private readonly IRulesProvider _rulesProvider;
        private readonly Func<SaveRuleMessage, SaveRuleViewModel> _saveRuleFactory;
        private readonly Func<LoadRuleMessage, LoadRuleViewModel> _loadRuleFactory;

        public LogViewModel Log { get; }
        public SnackbarService Snackbar { get; }

        public ShellViewModel(
            IFileService fileService,
            ISyntaxHighlightingManager syntaxHighlightingManager,
            LogViewModel log,
            ILogService logService,
            IEventAggregator eventAggregator,
            SnackbarService snackbarService,
            IDialogService dialogService,
            Func<ProgressDialogViewModel> progressDialogFactory,
            IRulesProvider rulesProvider,
            Func<SaveRuleMessage, SaveRuleViewModel> saveRuleFactory,
            Func<LoadRuleMessage, LoadRuleViewModel> loadRuleFactory)
        {
            _fileService = fileService;
            _syntaxHighlightingManager = syntaxHighlightingManager;
            _logService = logService;

            _eventAggregator = eventAggregator;
            _snackbarService = snackbarService;
            _dialogService = dialogService;
            _progressDialogFactory = progressDialogFactory;
            _rulesProvider = rulesProvider;
            _saveRuleFactory = saveRuleFactory;
            _loadRuleFactory = loadRuleFactory;

            Log = log;

            LineTransformers = new BindableCollection<IVisualLineTransformer>();
            SelectedSyntax = _syntaxHighlightingManager.GetSyntaxTransformers().FirstOrDefault();

            SyntaxDefinitionsCollectionView =
                CollectionViewSource.GetDefaultView(_syntaxHighlightingManager.GetSyntaxTransformers());

            ElementGenerators =
                new BindableCollection<VisualLineElementGenerator>(new[] { new LogBeginningElementGenerator(), });
            Snackbar = snackbarService;
            _eventAggregator.Subscribe(this);

        }

        private bool _isRightDrawerOpen;
        public bool IsRightDrawerOpen
        {
            get
            {
                return _isRightDrawerOpen;
            }
            set
            {
                _isRightDrawerOpen = value;
                NotifyOfPropertyChange();
            }
        }

        private bool _isSelectionContextMenuActive;
        public bool IsSelectionContextMenuActive
        {
            get { return _isSelectionContextMenuActive; }
            set
            {
                _isSelectionContextMenuActive = value;
                NotifyOfPropertyChange();
            }
        }




        public ICollectionView SyntaxDefinitionsCollectionView { get; }

        public IEnumerable<ISyntaxTransformer> SyntaxDefinitions => _syntaxHighlightingManager
            .GetSyntaxTransformers();

        private ISyntaxTransformer _selectedSyntax;
        public ISyntaxTransformer SelectedSyntax
        {
            get
            {
                return _selectedSyntax;
            }
            set
            {
                if (_selectedSyntax == value)
                {
                    return;
                }

                if (_selectedSyntax != null)
                {
                    LineTransformers.Remove(_selectedSyntax);
                }

                if (value != null)
                {
                    LineTransformers.Add(value);
                }

                _selectedSyntax = value;
                NotifyOfPropertyChange(() => SelectedSyntax);
            }
        }

        private IObservableCollection<IVisualLineTransformer> _lineTransformers;

        public IObservableCollection<IVisualLineTransformer> LineTransformers
        {
            get { return _lineTransformers; }
            set
            {
                _lineTransformers = value;
                NotifyOfPropertyChange();
            }
        }

        private bool _isSyntaxPopupOpen;
        public bool IsSyntaxPopupOpen
        {
            get { return _isSyntaxPopupOpen; }
            set
            {
                _isSyntaxPopupOpen = value;
                NotifyOfPropertyChange();
            }
        }

        public void OpenSyntaxPopup()
        {
            IsSyntaxPopupOpen = true;
        }

        public void OpenFilters()
        {
            IsRightDrawerOpen = true;
        }

        private BindableCollection<VisualLineElementGenerator> _elementGenerators;

        public BindableCollection<VisualLineElementGenerator> ElementGenerators
        {
            get { return _elementGenerators; }
            set
            {
                _elementGenerators = value;
                NotifyOfPropertyChange();
            }
        }

        public async Task ShowBeforeSelected()
        {
            var entry = Log.LogResult.GetLogFromLine(Log.Document.Selection.StartPosition.Line);
            if (entry == null)
            {
                return;
            }

            Log.Root.Add(new IdRule() { Name = $"Id <= {entry.Id}", Id = entry.Id, SelectedAction = IdRuleAction.Before });
            await Log.Filter();
        }
        public async Task ShowAfterSelected()
        {
            var entry = Log.LogResult.GetLogFromLine(Log.Document.Selection.StartPosition.Line);
            if (entry == null)
            {
                return;
            }

            Log.Root.Add(new IdRule() { Name = $"Id >= {entry.Id}", Id = entry.Id, SelectedAction = IdRuleAction.After });
            await Log.Filter();

        }
        public async Task ShowOnlySelected()
        {
            if (Log.Document.Selection.IsMultiline || Log.Document.Selection.IsEmpty)
            {
                return;
            }

            Log.Root.Add(new TextRule() { Name = $"Only '{Log.Document.Selection.GetText()}'", Text = Log.Document.Selection.GetText(), SelectedAction = TextRuleAction.Contains });
            await Log.Filter();

        }
        public async Task HideSelected()
        {
            if (Log.Document.Selection.IsMultiline || Log.Document.Selection.IsEmpty)
            {
                return;
            }

            Log.Root.Add(new TextRule() { Name = $"Not '{Log.Document.Selection.GetText()}'", Text = Log.Document.Selection.GetText(), SelectedAction = TextRuleAction.DoesNotContain });
            await Log.Filter();

        }
        public async Task OpenManyFiles()
        {
            var files = _fileService.OpenMany();
            if (files.Length == 0)
            {
                return;
            }

            await LoadFiles(files);
        }

        public void Close(Window window) => window.Close();
        public void Minimize(Window window) => window.WindowState = WindowState.Minimized;
        public void Maximize(Window window) =>
            window.WindowState = window.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;

        public void Handle(SnackbarMessage message)
        {
            message.Action(_snackbarService.Queue);
        }

        public async void Handle(LoadRuleMessage message)
        {
            var loadRuleVm = _loadRuleFactory(message);
            var loadTask = loadRuleVm.LoadAsync();
            await _dialogService.ShowDialogAsync(loadRuleVm).ContinueWith(t =>
            {
                if (!loadTask.IsCompleted)
                {
                    //cancel
                }
            });

        }

        public async void Handle(SaveRuleMessage message)
        {
            var saveRuleVm = _saveRuleFactory(message);
            await _dialogService.ShowDialogAsync(saveRuleVm);
            //var result = _rulesProvider.Add(new RuleInfo()
            //{
            //    Author = Environment.UserName,
            //    DateUpdated = DateTime.UtcNow,
            //    DateCreated = DateTime.UtcNow,
            //    Group = "local",
            //    Id = Guid.NewGuid(),
            //    Rule = message.Rule
            //});
        }

        public void DragOver(IDropInfo dropInfo)
        {
            var dataObject = dropInfo.Data as IDataObject;
            var data = dataObject?.GetData(DataFormats.FileDrop, false);

            if (data == null)
            {
                return;
            }

            dropInfo.Effects = DragDropEffects.Copy;
        }

        public async void Drop(IDropInfo dropInfo)
        {
            var dataObject = dropInfo.Data as IDataObject;
            var data = dataObject?.GetData(DataFormats.FileDrop, false);

            if (data == null)
            {
                return;
            }

            var paths = ((string[])data).Where(File.Exists).ToArray();
            await LoadFiles(paths);
        }

        private async Task LoadFiles(string[] paths)
        {
            var streams = _fileService.OpenMany(paths);

            var progressDialog = _progressDialogFactory();

            progressDialog.Message = $"Loading files...";

            var dialogTask = _dialogService.ShowDialogAsync(progressDialog);
            var log = await _logService.LoadAsync(streams)
                .ContinueWith(t =>
                {
                    progressDialog.Close();
                    return t.Result;
                }, TaskScheduler.FromCurrentSynchronizationContext());
            await dialogTask;

            await Log.SetLog(log);
            (ElementGenerators.FirstOrDefault() as LogBeginningElementGenerator).LogResult = Log.LogResult;

            IsSelectionContextMenuActive = Log.LogResult.Entries.Any();
            _eventAggregator.PublishOnCurrentThread(new SnackbarMessage() { Action = q => q.Enqueue($"{paths.Length} files loaded") });
        }
    }
}