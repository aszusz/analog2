﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ViewModels.LineTransformers;

namespace ViewModels.Services
{
    public class DefaultSyntaxHighlightingManager : ISyntaxHighlightingManager
    {
        public DefaultSyntaxHighlightingManager()
        {
            _syntaxDefinitions = new Lazy<ISyntaxTransformer[]>(() => new ISyntaxTransformer[] { new PlainTextTransformer(), new AcwLineTransformer() });
        }

        private readonly Lazy<ISyntaxTransformer[]> _syntaxDefinitions;

        public IEnumerable<ISyntaxTransformer> GetSyntaxTransformers() =>
            _syntaxDefinitions.Value;
    }
}
