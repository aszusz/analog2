﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using LiteDB;
using Models.Rules;
using ViewModels.Helpers;

namespace ViewModels.Services
{
    public class LiteDbRulesProvider : IRulesProvider
    {
        private readonly LiteDbMapper _mapper;
        public LiteDbRulesProvider(LiteDbMapper mapper)
        {
            _mapper = mapper;
            Name = "local (LiteDb)";
        }

        public string Name { get; }
        public Task<IList<RuleInfo>> Load()
        {
            _mapper.EnsureRegistered();
            using (var db = new LiteDatabase(@"filename=analog.db; journal=false"))
            {
                var collection = db.GetCollection<RuleInfo>("rules");
                return Task.FromResult<IList<RuleInfo>>(collection.FindAll().ToList());
            }
        }

        public Task<bool> Update(RuleInfo ruleInfo)
        {
            _mapper.EnsureRegistered();
            using (var db = new LiteDatabase(@"filename=analog.db; journal=false"))
            {
                var collection = db.GetCollection<RuleInfo>("rules");
                return Task.FromResult(collection.Update(ruleInfo));
            }
        }

        public Task<bool> Remove(Guid ruleId)
        {
            _mapper.EnsureRegistered();
            using (var db = new LiteDatabase(@"filename=analog.db; journal=false"))
            {
                var collection = db.GetCollection<RuleInfo>("rules");
                return Task.FromResult(collection.Delete(ri => ri.Id == ruleId) > 0);

            }
        }

        public Task<bool> Add(RuleInfo ruleInfo)
        {
            _mapper.EnsureRegistered();
            using (var db = new LiteDatabase(@"filename=analog.db; journal=false"))
            {
                var collection = db.GetCollection<RuleInfo>("rules");
                collection.Insert(ruleInfo);
            }
            return Task.FromResult(true);

        }
    }
}