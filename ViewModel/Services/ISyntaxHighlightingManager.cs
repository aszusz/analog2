﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.LineTransformers;

namespace ViewModels.Services
{
    public interface ISyntaxHighlightingManager
    {
        IEnumerable<ISyntaxTransformer> GetSyntaxTransformers();
    }
}
