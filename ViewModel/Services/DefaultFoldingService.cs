﻿using System.Collections.Generic;
using System.Linq;
using Models;

namespace ViewModels.Services
{
    public class DefaultFoldingService : IFoldingService
    {
        public IEnumerable<Folding> Update(IEnumerable<LogEntry> logEntries)
        {
            var offset = 0;

            return logEntries.Select(x =>
            {
                var first = x.ToString().IndexOf('\n');
                var last = x.ToString().LastIndexOf('\n');
                if (first == last)
                {
                    offset += x.ToString().Length;
                    return null;
                }
                var result = new
                {
                    Start = offset + first,
                    End = offset + last - 1
                };
                offset += x.ToString().Length;
                return result;
            }).Where(x => x != null)
                .Select(x => new Folding()
                {
                    StartOffset = x.Start,
                    EndOffset = x.End,
                    Name = "...",
                    IsFolded = false //for unknown reason it keeps crashing if set to true :(
                });
        }
    }
}