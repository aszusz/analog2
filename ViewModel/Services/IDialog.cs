﻿using System;

namespace ViewModels.Services
{
    public interface IDialog
    {
        event EventHandler OnClose;
        void Close();
    }
}