﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MaterialDesignThemes.Wpf;

namespace ViewModels.Services
{
    public class DialogService : IDialogService
    {
        public async Task ShowDialogAsync(object viewModel)
        {
            await DialogHost.Show(viewModel);
        }

        public async Task<T> ShowDialogAsync<T>(object viewModel)
        {
            var result = default(T);
            await DialogHost.Show(viewModel, (s, e) => result = (T)e.Parameter);
            return result;
        }

        public async Task ShowDialogAsync(IDialog dialogViewModel)
        {
            IInputElement inputElement = null;
            EventHandler onClose = new EventHandler((s, e) => DialogHost.CloseDialogCommand.Execute(s, inputElement));
            await DialogHost.Show(dialogViewModel,
                 (s, e) => { inputElement = ((IInputElement)e.OriginalSource); dialogViewModel.OnClose += onClose; }, (s, e) => dialogViewModel.OnClose -= onClose);

        }
    }
}
