﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ICSharpCode.AvalonEdit.Rendering;
using Models;

namespace ViewModels.ElementGenerators
{
    public class LogBeginningElementGenerator : VisualLineElementGenerator
    {
        public LogResult LogResult { get; set; }

        public override int GetFirstInterestedOffset(int startOffset)
        {
            if (LogResult == null || !LogResult.Entries.Any())
                return -1;

            return LogResult.IsLineStartingLogEntry(CurrentContext.VisualLine.FirstDocumentLine.LineNumber) &&
                   CurrentContext.VisualLine.StartOffset == startOffset
                ? startOffset
                : -1;
        }

        public override VisualLineElement ConstructElement(int offset)
        {
            var log = LogResult.GetLogFromLine(CurrentContext.VisualLine.FirstDocumentLine.LineNumber);
            return new InlineObjectElement(0, new ContentControl()
            {
                Content = new LogBeginning()
                {
                    File = Path.GetFileName(log.LogFile.Path),
                    Id = log.Id
                },
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            });
        }
    }
}