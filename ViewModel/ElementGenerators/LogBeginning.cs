﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels.ElementGenerators
{
    public class LogBeginning
    {
        public string File { get; set; }
        public long Id { get; set; }
    }
}
