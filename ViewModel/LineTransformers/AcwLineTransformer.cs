﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;

namespace ViewModels.LineTransformers
{
    public class AcwLineTransformer : DocumentColorizingTransformer, ISyntaxTransformer
    {
        public string Name => "ACW";
        protected override void ColorizeLine(DocumentLine line)
        {
            var text = CurrentContext.Document.GetText(line);

            //if (text.Length >= 19)
            //{
            //    var datePart = text.Substring(0, 19);
            //    DateTime dateResult;
            //    if (DateTime.TryParse(datePart, out dateResult))
            //    {
            //        ChangeLinePart(line.Offset, line.Offset + 19,
            //            x =>
            //            {
            //                x.TextRunProperties.SetTextDecorations(
            //                    new TextDecorationCollection(new TextDecoration[]
            //                        {new TextDecoration() {Location = TextDecorationLocation.Underline}}));
                            
            //            });
            //    }
            //}

            if (text.IndexOf("[ERROR]", StringComparison.InvariantCulture) != -1)
            {
                ChangeLinePart(line.Offset, line.EndOffset, x => x.TextRunProperties.SetForegroundBrush(new SolidColorBrush(Colors.Red)));

                ChangeLinePart(line.Offset, line.EndOffset, x =>
                {
                    var tf = new Typeface(x.TextRunProperties.Typeface.FontFamily, x.TextRunProperties.Typeface.Style,
                        FontWeights.Bold, x.TextRunProperties.Typeface.Stretch);
                    x.TextRunProperties.SetTypeface(tf);
                });
                return;
            }
            if (text.IndexOf("[WARNING]", StringComparison.InvariantCulture) != -1)
            {
                ChangeLinePart(line.Offset, line.EndOffset, x => x.TextRunProperties.SetForegroundBrush(new SolidColorBrush(Colors.Orange)));
                ChangeLinePart(line.Offset, line.EndOffset, x =>
                {
                    var tf = new Typeface(x.TextRunProperties.Typeface.FontFamily, x.TextRunProperties.Typeface.Style,
                        FontWeights.Bold, x.TextRunProperties.Typeface.Stretch);
                    x.TextRunProperties.SetTypeface(tf);
                });
                return;
            }

            var infoOffset = text.IndexOf("[INFO]", StringComparison.InvariantCulture);
            if (infoOffset != -1)
            {
                ChangeLinePart(line.Offset + infoOffset, line.Offset + infoOffset + 6, x => x.TextRunProperties.SetForegroundBrush(new SolidColorBrush(Colors.Green)));
                return;
            }

            var debugOffset = text.IndexOf("[DEBUG]", StringComparison.InvariantCulture);
            if (debugOffset != -1)
            {
                ChangeLinePart(line.Offset + debugOffset, line.Offset + debugOffset + 7, x => x.TextRunProperties.SetForegroundBrush(new SolidColorBrush(Colors.Green)));
            }


        }
    }
}
