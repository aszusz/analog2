﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Models.Rules;

namespace ViewModels.Rules
{
    public class RuleInfoViewModel : PropertyChangedBase
    {
        public RuleInfo RuleInfo { get; }

        public RuleInfoViewModel(RuleInfo ruleInfo)
        {
            RuleInfo = ruleInfo;
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
