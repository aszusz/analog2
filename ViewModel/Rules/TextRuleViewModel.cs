using System;
using System.ComponentModel;
using Models.Rules;
using ViewModels.Editors;
using ViewModels.Helpers;

namespace ViewModels.Rules
{
    public class TextRuleViewModel : RuleViewModelBase
    {
        private readonly TextRule _rule;

        public TextRuleViewModel(TextRule rule)
            : base(rule)
        {
            _rule = rule;
            RefreshName();
        }

        [Expose(OverridenType = typeof(Enum))]
        [DisplayName("Action")]
        public TextRuleAction SelectedAction
        {
            get { return _rule.SelectedAction; }
            set
            {
                _rule.SelectedAction = value;
                NotifyOfPropertyChange();
                RefreshName();
            }
        }

        [Expose]
        [DisplayName("Case sensitive")]
        public bool IsCaseSensitive
        {
            get { return _rule.IsCaseSensitive; }
            set
            {
                if (value == _rule.IsCaseSensitive) return;
                _rule.IsCaseSensitive = value;
                NotifyOfPropertyChange();
            }

        }

        [Expose]
        public string Text
        {
            get { return _rule.Text; }
            set
            {
                if (value == _rule.Text) return;
                _rule.Text = value;
                NotifyOfPropertyChange();
                RefreshName();
            }
        }

        private void RefreshName()
        {
            Name = $"{SelectedAction.GetDescription()} '{Text}'";
        }
    }

   
}