using System;
using Models.Rules;
using ViewModels.Editors;
using ViewModels.Helpers;

namespace ViewModels.Rules
{
    public class IdRuleViewModel : RuleViewModelBase
    {
        private readonly IdRule _rule;

        public IdRuleViewModel(IdRule rule) :
            base(rule)
        {
            _rule = rule;
            RefreshName();
        }

        [Expose(OverridenType = typeof(Enum))]
        public IdRuleAction SelectedAction
        {
            get { return _rule.SelectedAction; }
            set
            {
                _rule.SelectedAction = value;
                NotifyOfPropertyChange();
                RefreshName();
            }
        }
        [Expose(OverridenType = typeof(int))]
        public long Id
        {
            get { return _rule.Id; }
            set
            {
                _rule.Id = value;
                NotifyOfPropertyChange();
                RefreshName();
            }
        }

        private void RefreshName()
        {
            Name = $"Id {SelectedAction.GetDescription()} {Id}";
        }
    }
}