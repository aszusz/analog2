﻿using System;
using Caliburn.Micro;
using GongSolutions.Wpf.DragDrop;
using Models.Rules;
using ViewModels.Editors;

namespace ViewModels.Rules
{
    public abstract class RuleViewModelBase : PropertyChangedBase

    {
        public IRule Rule { get; }


        private RuleParentViewModelBase _parent;

        public RuleParentViewModelBase Parent
        {
            get { return _parent; }
            set
            {
                _parent = value;
                NotifyOfPropertyChange();
            }
        }

        public RuleViewModelBase(IRule rule)
        {
            Rule = rule;
        }


        public virtual string Name
        {
            get { return Rule.Name; }
            set
            {
                Rule.Name = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsEnabled
        {
            get { return Rule.IsEnabled; }
            set
            {
                Rule.IsEnabled = value;
                NotifyOfPropertyChange();
            }
        }

        public virtual void RevertIsEnabled()
        => IsEnabled = !IsEnabled;

    }
}