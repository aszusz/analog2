﻿using System;
using Caliburn.Micro;
using ViewModels.Services;

namespace ViewModels
{
    public class ProgressDialogViewModel : PropertyChangedBase, IDialog
    {
        public event EventHandler OnClose;
        public void Close()
        {
            OnClose?.Invoke(this, EventArgs.Empty);
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
