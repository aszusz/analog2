using Autofac;
using MaterialDesignThemes.Wpf;
using Models;
using ViewModels.Editors;
using ViewModels.Rules;
using ViewModels.Services;

namespace ViewModels
{
    public class ViewModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellViewModel>()
                .SingleInstance();

            builder.RegisterType<LogViewModel>()
                .SingleInstance();

            builder.RegisterType<FileService>()
                .As<IFileService>()
                .SingleInstance();

            builder.RegisterType<DefaultSyntaxHighlightingManager>()
                .As<ISyntaxHighlightingManager>()
                .SingleInstance();

            builder.RegisterType<EditorFactory>()
                .As<IEditorFactory>()
                .SingleInstance();

            builder.RegisterType<RuleViewModelFactory>();

            builder.RegisterType<JsonRulesSerializer>()
                .As<IRulesSerializer>();

            builder.RegisterType<LogService>()
                .As<ILogService>();

            builder.RegisterType<LiteDbRulesProvider>()
                .As<IRulesProvider>();

            builder.RegisterType<SnackbarMessageQueue>();

            builder.RegisterType<LiteDbMapper>()
                .SingleInstance();


            builder.RegisterType<SaveRuleViewModel>();
            builder.RegisterType<LoadRuleViewModel>();
            builder.RegisterType<RuleInfoViewModel>();


            builder.RegisterType<SnackbarService>();
            builder.RegisterType<ProgressDialogViewModel>();
            builder.RegisterType<ProgressDialogViewModel>().As<IDialog>();
            builder.RegisterType<DialogService>().As<IDialogService>();
            builder.RegisterType<Document>();
        }
    }
}