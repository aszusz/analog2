﻿using System.Collections.Generic;
using Caliburn.Micro;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ViewModels.Services;

namespace ViewModels
{
    public class Document : PropertyChangedBase
    {
        public Document()
        {
            TextDocument = new TextDocument();
        }

        private Selection _selection;
        public Selection Selection
        {
            get { return _selection; }
            set
            {
                _selection = value;
                NotifyOfPropertyChange();
            }
        }

        private IList<Folding> _foldings;
        public IList<Folding> Foldings
        {
            get { return _foldings; }
            set
            {
                _foldings = value;
                NotifyOfPropertyChange();
            }
        }

        public TextDocument TextDocument { get; }

    }
}
