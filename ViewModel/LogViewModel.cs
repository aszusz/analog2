﻿using System;
using Caliburn.Micro;
using Models;
using Models.Rules;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using GongSolutions.Wpf.DragDrop;
using ViewModels.Editors;
using ViewModels.Rules;
using ViewModels.Services;

namespace ViewModels
{
    public class LogViewModel : PropertyChangedBase, IDropTarget
    {
        private readonly LogService _logService;
        private readonly IFoldingService _foldingService;
        private readonly IEditorFactory _editorFactory;

        private RuleParentViewModelBase _root;
        private Log _log;

        public Document Document { get; }

        public LogViewModel(LogService logService,
            Document document,
            RuleViewModelFactory ruleViewModelFactory,
            IFoldingService foldingService,
            Func<LogViewModel, IEditorFactory> editorFactory)
        {
            _logService = logService;
            _foldingService = foldingService;
            _editorFactory = editorFactory(this);
            Document = document;
            Root = (CompositeRuleViewModel)ruleViewModelFactory.Create(new CompositeRule() { Name = "Root" }, null);
            TreeRoot = new BindableCollection<RuleViewModelBase>() { Root };
            RuleProperties = new BindableCollection<IEditor>(_editorFactory.Create(Root));

        }

        public RuleParentViewModelBase Root
        {
            get { return _root; }
            set
            {
                if (Equals(value, _root)) return;
                _root = value;
                NotifyOfPropertyChange(() => Root);
            }
        }
        public BindableCollection<RuleViewModelBase> TreeRoot { get; }


        private BindableCollection<IEditor> _ruleProperties;
        public BindableCollection<IEditor> RuleProperties
        {
            get
            {
                return _ruleProperties;
            }
            private set
            {
                _ruleProperties = value;
                NotifyOfPropertyChange();
            }
        }

        public async Task Filter()
        {
            var filter = Root.Rule.GetFilter();
            LogResult = await _logService.FilterAsync(_log, filter);

            Document.TextDocument.Text = LogResult.LogText;
            Document.TextDocument.UndoStack.ClearAll();
            Document.Foldings = _foldingService
                .Update(LogResult.Entries)
                .ToList();
        }

        public async Task SetLog(Log log)
        {
            _log = log;
            await Filter();
        }

        private LogResult _logResult;
        public LogResult LogResult
        {
            get
            {
                return _logResult;
            }
            private set
            {
                _logResult = value;
                NotifyOfPropertyChange();
            }
        }

        public void DeleteRule(RuleViewModelBase rule)
        {
            rule.Parent.Remove(rule);
        }



        public void ShowRuleProperties(RoutedPropertyChangedEventArgs<object> eArgs)
        {
            var rule = eArgs.NewValue as RuleViewModelBase;
            if (rule == null)
            {
                return;
            }

            if (RuleProperties != null)
            {
                foreach (var ruleProperty in RuleProperties)
                {
                    ruleProperty.Dispose();
                }
            }
            RuleProperties = new BindableCollection<IEditor>(_editorFactory.Create(rule));
        }

        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            var sourceItem = dropInfo.Data as RuleViewModelBase;
            var targetItem = dropInfo.TargetItem as RuleViewModelBase;

            if (sourceItem.Parent == null)
            {
                dropInfo.Effects = DragDropEffects.None;
                return;
            }

            if (sourceItem != null && targetItem != null)
            {
                dropInfo.DropTargetAdorner = (targetItem is RuleParentViewModelBase && (dropInfo.InsertPosition & RelativeInsertPosition.TargetItemCenter) != 0)
                    ? DropTargetAdorners.Highlight
                    : DropTargetAdorners.Insert;

                dropInfo.Effects = DragDropEffects.Move;
            }
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            var sourceItem = dropInfo.Data as RuleViewModelBase;
            var targetItem = dropInfo.TargetItem as RuleViewModelBase;

            if (targetItem is RuleParentViewModelBase && (dropInfo.InsertPosition & RelativeInsertPosition.TargetItemCenter) != 0)
            {
                var parent = targetItem as RuleParentViewModelBase;
                parent.Add(sourceItem.Rule);

                sourceItem.Parent.Remove(sourceItem, false);
                sourceItem.Parent = targetItem as RuleParentViewModelBase;
                return;
            }

            targetItem.Parent.Insert(dropInfo.InsertIndex, sourceItem.Rule);
            sourceItem.Parent.Remove(sourceItem, false);
        }
    }
}