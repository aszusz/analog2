using System;
using System.Linq;
using Models;

namespace ViewModels.Editors
{
    public sealed class DateTimeEditor : EditorBase
    {
        private readonly LogViewModel _logViewModel;

        public DateTimeEditor(LogViewModel logViewModel)
        {
            _logViewModel = logViewModel;
        }


        public DateTime? StartDate => _logViewModel?.LogResult?.Entries?.FirstOrDefault()?.Date;
        public DateTime? EndDate => _logViewModel?.LogResult?.Entries?.LastOrDefault()?.Date;

    }
}